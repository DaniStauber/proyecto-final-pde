//+
Point(1) = {-50, 0, 0, 1.0};
//+
Point(2) = {0, 0, 0, 0.0001};
//+
Point(3) = {50, 0, 0, 1};
//+
Point(4) = {50, 50, 0, 1};
//+
Point(5) = {0, 50, 0, 0.0001};

//+
Point(6) = {-50, 50, 0, 1};


//+

Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 1};
//+
Curve Loop(1) = {4, 5, 6, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Surface("int", 7) = {1};
//+
Physical Curve("intz", 8) = {1, 2};
//+
Physical Curve("extrhoficc", 9) = {6};
//+
Physical Curve("extrho", 10) = {3};
//+
Physical Curve("extz", 11) = {4, 5};
