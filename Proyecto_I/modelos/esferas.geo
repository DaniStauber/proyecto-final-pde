// Gmsh project created on Fri Oct 15 00:43:03 2021
SetFactory("OpenCASCADE");

//+
Sphere(1) = {0., 0, 0, 100., -Pi/2, Pi/2, 2*Pi};
//+
Sphere(2) = {0., 0, 0, 12., -Pi/2, Pi/2, 2*Pi}; 
//+

Surface Loop(3) = {1};
//+
Surface Loop(4) = {2}; 

//+
//Volume(1) = {1};
//+
//Volume(2) = {2};

//+
BooleanDifference(5) = { Volume{1}; Delete; }{ Volume{2}; Delete; };
//+
Physical Volume("volume") = {5};

//+
Physical Surface("ext", 7) = {1};
//+
Physical Surface("int", 8) = {2};
