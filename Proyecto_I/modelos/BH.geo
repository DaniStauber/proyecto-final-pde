//+
SetFactory("OpenCASCADE");
Box(1) = {-100, -100, -100, 200, 200, 200};
//+
Sphere(2) = {0., 0., 0., 10, -Pi/2, Pi/2, 2*Pi};
//+
Surface Loop(3) = {1};
//+
Surface Loop(4) = {2}; 


//+
//Volume(1) = {1};
//+
//Volume(2) = {2};

BooleanDifference(5) = { Volume{1}; Delete; }{ Volume{2}; Delete; };
//+
Physical Volume("volume") = {5};



//+
Physical Surface("Ext", 16) = {4, 3, 6, 5, 2, 1};
//+
Physical Surface("int", 17) = {7};
