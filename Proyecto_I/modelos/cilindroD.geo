// Gmsh project created on Fri Oct 15 00:43:03 2021
SetFactory("OpenCASCADE");
//+
Cylinder(1) = {0, 0, 0, 1, 0, 0, 0.5, 2*Pi}; //ext
//+
Cylinder(2) = {0, 0, 0, 1, 0, 0, 0.25, 2*Pi}; //int
//+

Surface Loop(3) = {1};
//+
Surface Loop(4) = {2}; 

//+
//Volume(1) = {1};
//+
//Volume(2) = {2};


//+
BooleanDifference(5) = { Volume{1}; Delete; }{ Volume{2}; Delete; };
//+
Physical Volume("volume") = {5};
//+
//+
Physical Surface("tapas", 7) = {3, 2};
//+
Physical Surface("int", 8) = {4};
//+
Physical Surface("ext", 9) = {1};
