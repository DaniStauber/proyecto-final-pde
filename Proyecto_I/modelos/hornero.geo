// -----------------------------------------------------------------------------
//
//  Gmsh GEO 
 
lc = 1e-1/2; 

// Puntos cuadrado:

Point(1) = {0, 0, 0, lc};
Point(2) = {2.0, 0,  0, lc};
Point(3) = {2.0, 2.0, 0, lc};
Point(4) = {0,  2.0, 0, lc};
Point(5) = {1.,  3.0, 0, lc};

//Puntos para el circulo

Point(6) = {1.0, 0.5, 0, lc};
Point(7) = {0.5, 1.,  0, lc};
Point(8) = {1.0, 1.5, 0, lc};
Point(9) = {1.5,  1., 0, lc};
Point(10) = {1.0,  1.0, 0, lc}; 


//Lineas del exterior

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 5};
Line(4) = {5, 4};
Line(5)= {4, 1};

//Lineas del circulo
Circle(6)={6,10,9};
Circle(7)={9,10,8};
Circle(8)={8,10,7};
Circle(9)={7,10,6};

// The hornero

Curve Loop(20)={1, 2, 3, 4, 5};


//#the inner circle

Curve Loop(21)={ 6, 7, 8, 9}; 

Plane Surface(100)={20,21};

Physical Surface("surface",5) = {100};

Physical Curve("circle",2) = {6, 7, 8, 9};

Physical Curve("ext",4) = {1,2,3,4,5};

Mesh 2; 


Save "hornero.msh";