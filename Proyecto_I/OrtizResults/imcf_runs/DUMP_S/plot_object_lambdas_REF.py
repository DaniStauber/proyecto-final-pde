# encoding: utf-8
import numpy as np
import matplotlib.pyplot as plt

REF = raw_input('Ingrese el numero de referencia: ' )
data = open(REF+"_lambdas_object_surface", "r")
datos = data.readlines()
data.close()

LEN = len(datos)/3
for i in range(LEN):
    theta = datos[3*i].split()
    lambda_phi = datos[3*i+1].split()
    lambda_theta = datos[3*i+2].split()
    plt.ylim((-1.0e-10,1.0e-10))
    plt.xlim((0.0,0.5*np.pi))
    plt.plot(theta,lambda_phi, '--')
    plt.plot(theta,lambda_theta, '-')
#    plt.title(r'$t = %.2f$' % (t))
#    plt.legend([r'$\chi_{22}(t = %.2f)$' % (t), r'$\chi_{33}(t = %.2f)$' % (t)], loc=0)
plt.savefig('SUN_object_lambdas.pdf')
plt.show()

