# encoding: utf-8
import numpy as np
import matplotlib.pyplot as plt

REF = raw_input('Ingrese el numero de referencia: ' )
data = open(REF+"_lambdas_object_surface", "r")
datos = data.readlines()
data.close()

# adjust the size for paper
fig = plt.figure(figsize=(4,3))
ax = fig.add_subplot(111)
plt.subplots_adjust(left=0.18, bottom=0.14, right=0.9, top=0.9, wspace=0, hspace=0)

LEN = len(datos)/3
for i in range(LEN):
    theta = datos[3*i].split()
    lambda_phi = datos[3*i+1].split()
    lambda_theta = datos[3*i+2].split()
    plt.ylim((6.6e-7,7.2e-7))
    ax.set_yticks([6.6e-7, 6.8e-7, 7.0e-7, 7.2e-7])
    ax.set_yticklabels([r'$6.6$', r'$6.8$', r'$7.0$', r'$7.2$'])
    ax.set_xticks([0.0, 0.25*np.pi, 0.5*np.pi])
    ax.set_xticklabels([r'$0$', r'$\pi/4$', r'$\pi/2$'])
    plt.plot(theta,lambda_phi, '--')
    plt.plot(theta,lambda_theta, '-')
    plt.ylabel(r'$[10^{-7} {\rm cm}^{-1}]$')
#    plt.xlabel(r'$\theta$')
#    plt.title(r'$t = %.2f$' % (t))
#    plt.legend([r'$\chi_{22}(t = %.2f)$' % (t), r'$\chi_{33}(t = %.2f)$' % (t)], loc=0)
plt.savefig('NS_object_lambdas.pdf')
plt.show()

