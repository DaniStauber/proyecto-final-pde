# encoding: utf-8
import numpy as np
import matplotlib.pyplot as plt

# Spheroid support parameters of the object 
#OBJECT_R_MAX = 0.75e+06
#OBJECT_Z_MAX = 0.73e+06
# Here choose the largest between R_MAX and Z_MAX
#a = OBJECT_R_MAX;
# Define la escala relativa a "a" para el tamano del grafico (tipica: 2.0)
#escala = 4.0

# Draw the object's surface
#v = np.linspace(0.0, 0.5*np.pi, 100)
#Xobj = OBJECT_R_MAX*np.sin(v)
#Zobj = OBJECT_Z_MAX*np.cos(v)

REF = raw_input('Ingrese el numero de referencia: ' )
data = open(REF+"_sol", "r")
datos = data.readlines()
data.close()

# Plots start
left = 0.16
bottom = 0.14 
width = 0.8
height = 0.8
plt.figure(figsize=(4.0,4.0))
ax = plt.axes([left, bottom, width, height])
ax.set_aspect('equal')
plt.xlim(0.0, 3.7e+6)
plt.ylim(0.0, 3.7e+6)
plt.xlabel(r'$\rho\;[10^6 {\rm cm}]$')
plt.ylabel(r'$z\;[10^6 {\rm cm}]$')
plt.gca().set_aspect('equal', adjustable='box')
#labels = [item.get_text() for item in ax.get_xticklabels()]
#labels = [r'$0$', r'$0.5$', r'$1.0$', r'$1.5$', r'$2.0$', r'$2.5$', r'$3.0$']
#labels = [r'$0.0$', r'$0.5\times 10^6$', r'$1.0\times 10^6$', r'$1.5\times 10^6$', r'$2.0\times 10^6$', r'$2.5\times 10^6$', r'$3.0\times 10^6$']
ax.set_xticks([0, 1.0e+6, 2.0e+6, 3.0e+6, 4.0e+6])
ax.set_xticklabels([r'$0$', r'$1.0$', r'$2.0$', r'$3.0$', r'$4.0$'])
ax.set_yticks([0, 1.0e+6, 2.0e+6, 3.0e+6, 4.0e+6])
ax.set_yticklabels([r'$0$', r'$1.0$', r'$2.0$', r'$3.0$', r'$4.0$'])
ax.annotate(r'$t=0$', xy=(0.2e+6,0.73e+6), xytext=(0.1e+6,0.3e+6))
ax.annotate(r'$t=0.5$', xy=(0.2e+6,1.0e+6), xytext=(0.2e+6,1.02e+6))
ax.annotate(r'$t=1.0$', xy=(0.2e+6,1.4e+6), xytext=(0.2e+6,1.42e+6))
ax.annotate(r'$t=1.5$', xy=(0.2e+6,1.9e+6), xytext=(0.2e+6,1.9e+6))
ax.annotate(r'$t=2.0$', xy=(0.2e+6,2.6e+6), xytext=(0.2e+6,2.53e+6))
ax.annotate(r'$t=2.5$', xy=(0.2e+6,3.3e+6), xytext=(0.2e+6,3.33e+6))

LEN = len(datos)/8
for i in range(LEN):
    t = float(datos[8*i])
    theta = datos[8*i+1].split()
    x_obj = datos[8*i+2].split()
    y_obj = datos[8*i+3].split()
    x = datos[8*i+4].split()
    y = datos[8*i+5].split()
    #plot del objeto
    plt.plot(x_obj,y_obj,'k')
    #plot de la superficie
    plt.plot(x,y)
#    plt.legend([r'Object surface', r'Sup. at $t = %.6f$' % (t)], loc=0)
#    plt.savefig('%d_surfaces_%d.pdf' % (int(REF),100*t))
    plt.savefig('O_surfaces.pdf')
#    plt.show()
# Comentar el plt.show() anterior y descomentar el siguiente para ver todas las superficies juntas
plt.show()



# Plots de Chi
fig = plt.figure()
fig.subplots_adjust(hspace=.5)

for i in range(LEN):
    t = float(datos[8*i])
    theta = datos[8*i+1].split()
    chi22 = datos[8*i+6].split()
    chi33 = datos[8*i+7].split()
    #plot de la curvatura extrinseca
    ax = fig.add_subplot(2,3,i+1)
#    labels = [item.get_text() for item in ax.get_yticklabels()]
#    labels = lables/1.0e+6
#    ax.set_yticklabels(labels)
    plt.xlim((0.0, 0.5*np.pi))
    ax.set_xticks([0.0, 0.25*np.pi, 0.5*np.pi])
    ax.set_xticklabels([r'$0$', r'$\pi/4$', r'$\pi/2$'])
    ax.set_yticks([0.0, 1.0e+6, 2.0e+6, 3.0e+6, 4.0e+6])
    ax.set_yticklabels([r'$0$', r'$1.0$', r'$2.0$', r'$3.0$', r'$4.0$'])
    plt.ylim((0.0,4.2e+6))
    plt.plot(theta,chi22, '--')
    plt.plot(theta,chi33, '-')
    plt.title(r'$t = %.2f$' % (t))
#    plt.legend([r'$\chi_{22}(t = %.2f)$' % (t), r'$\chi_{33}(t = %.2f)$' % (t)], loc=0)
plt.savefig('O_chis.pdf')
plt.show()




