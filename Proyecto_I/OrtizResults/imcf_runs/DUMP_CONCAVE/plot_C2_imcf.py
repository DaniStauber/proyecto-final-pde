# encoding: utf-8
import numpy as np
import matplotlib.pyplot as plt

# Spheroid support parameters of the object 
#OBJECT_R_MAX = 0.75e+06
#OBJECT_Z_MAX = 0.73e+06
# Here choose the largest between R_MAX and Z_MAX
#a = OBJECT_R_MAX;
# Define la escala relativa a "a" para el tamano del grafico (tipica: 2.0)
#escala = 4.0

# Draw the object's surface
#v = np.linspace(0.0, 0.5*np.pi, 100)
#Xobj = OBJECT_R_MAX*np.sin(v)
#Zobj = OBJECT_Z_MAX*np.cos(v)

REF = raw_input('Ingrese el numero de referencia: ' )
data = open(REF+"_sol", "r")
datos = data.readlines()
data.close()

# Plots start
left = 0.16
bottom = 0.14 
width = 0.8
height = 0.8
plt.figure(figsize=(4.0,4.0))
ax = plt.axes([left, bottom, width, height])
ax.set_aspect('equal')
plt.xlim(0.0, 1.6e+6)
plt.ylim(0.0, 1.6e+6)
plt.xlabel(r'$\rho\;[10^7 {\rm cm}]$')
plt.ylabel(r'$z\;[10^7 {\rm cm}]$')
plt.gca().set_aspect('equal', adjustable='box')
#labels = [item.get_text() for item in ax.get_xticklabels()]
#labels = [r'$0$', r'$0.5$', r'$1.0$', r'$1.5$', r'$2.0$', r'$2.5$', r'$3.0$']
#labels = [r'$0.0$', r'$0.5\times 10^6$', r'$1.0\times 10^6$', r'$1.5\times 10^6$', r'$2.0\times 10^6$', r'$2.5\times 10^6$', r'$3.0\times 10^6$']
#ax.set_xticks([0, 0.4e+7, 0.8e+7, 1.2e+7, 1.6e+7])
#ax.set_xticklabels([r'$0$', r'$0.4$', r'$0.8$', r'$1.2$', r'$1.6$'])
#ax.set_yticks([0, 0.4e+7, 0.8e+7, 1.2e+7, 1.6e+7])
#ax.set_yticklabels([r'$0$', r'$0.4$', r'$0.8$', r'$1.2$', r'$1.6$'])
#ax.annotate(r'$t=0$', xy=(0.2e+6,0.73e+6), xytext=(0.1e+6,0.3e+6))
#ax.annotate(r'$t=0.5$', xy=(0.2e+6,1.0e+6), xytext=(0.2e+6,1.02e+6))
#ax.annotate(r'$t=1.0$', xy=(0.2e+6,1.4e+6), xytext=(0.2e+6,1.42e+6))
#ax.annotate(r'$t=1.5$', xy=(0.2e+6,1.9e+6), xytext=(0.2e+6,1.9e+6))
#ax.annotate(r'$t=2.0$', xy=(0.2e+6,2.6e+6), xytext=(0.2e+6,2.53e+6))
#ax.annotate(r'$t=2.5$', xy=(0.2e+6,3.3e+6), xytext=(0.2e+6,3.33e+6))

LEN = len(datos)/12
for i in range(LEN):
    t = float(datos[12*i])
    theta = datos[12*i+1].split()
    x_obj = datos[12*i+2].split()
    y_obj = datos[12*i+3].split()
    x = datos[12*i+4].split()
    y = datos[12*i+5].split()
    #plot del objeto
    #ax.fill_between(x_obj, y_obj, 'k', alpha=0.5)
    plt.plot(x_obj,y_obj,'k')
    #plot de la superficie
    plt.plot(x,y)
#    plt.legend([r'Object surface', r'Sup. at $t = %.6f$' % (t)], loc=0)
#    plt.savefig('%d_surfaces_%d.pdf' % (int(REF),100*t))
#    plt.show()
# Comentar el plt.show() anterior y descomentar el siguiente para ver todas las superficies juntas
#plt.legend([r'$t=0.0$', r'$t=1.0$', r'$t=2.0$', r'$t=3.0$', r'$t=4.0$', r'$t=5.0$'], loc=0)
plt.savefig('C2_surfaces.pdf')
plt.show()


# Plots start
left = 0.16
bottom = 0.14 
width = 0.8
height = 0.8
plt.figure(figsize=(4.0,4.0))
ax = plt.axes([left, bottom, width, height])
ax.set_aspect('equal')
plt.xlabel(r'$\rho\;[10^5 {\rm cm}]$')
plt.ylabel(r'$z\;[10^5 {\rm cm}]$')
plt.xlim(0.0, 8.0e+5)
plt.ylim(0.0, 8.0e+5)
ax.set_xticks([0, 2.0e+5, 4.0e+5, 6.0e+5, 8.0e+5])
ax.set_xticklabels([r'$0$', r'$2.0$', r'$4.0$', r'$6.0$', r'$8.0$'])
ax.set_yticks([0, 2.0e+5, 4.0e+5, 6.0e+5, 8.0e+5])
ax.set_yticklabels([r'$0$', r'$2.0$', r'$4.0$', r'$6.0$', r'$8.0$'])

LEN = len(datos)/12
t = float(datos[0])
theta = datos[1].split()
x_obj = datos[2].split()
y_obj = datos[3].split()
x1 = datos[4].split()
y1 = datos[5].split()
x2 = datos[12+4].split()
y2 = datos[12+5].split()
x3 = datos[24+4].split()
y3 = datos[24+5].split()
#plot del objeto
#ax.fill_between(x_obj, y_obj, 'k', alpha=0.5)
plt.plot(x_obj,y_obj,'k', lw=2.0)
#plot de la superficie
plt.plot(x1, y1)
plt.plot(x2, y2)
plt.plot(x3, y3)
plt.annotate(r'object surface', xy=(1.2e+5,2.0e+5))
plt.annotate(r'$S_0$', xy=(0.5e+5,3.83e+5))
plt.annotate(r'$S_{0.1}$', xy=(1.0e+5,4.65e+5))
plt.annotate(r'$S_{0.2}$', xy=(2.0e+5,5.13e+5))
#    plt.legend([r'Object surface', r'Sup. at $t = %.6f$' % (t)], loc=0)
#    plt.savefig('%d_surfaces_%d.pdf' % (int(REF),100*t))
#    plt.show()
# Comentar el plt.show() anterior y descomentar el siguiente para ver todas las superficies juntas
#plt.legend([r'$t=0.0$', r'$t=1.0$', r'$t=2.0$', r'$t=3.0$', r'$t=4.0$', r'$t=5.0$'], loc=0)
plt.savefig('C2.pdf')
plt.show()



# Plots of Chi
fig = plt.figure()
fig.subplots_adjust(hspace=.5)

for i in range(LEN):
    t = float(datos[12*i])
    theta = datos[12*i+1].split()
    chi22 = datos[12*i+6].split()
    chi33 = datos[12*i+7].split()
    #plot de la curvatura extrinseca
    ax = fig.add_subplot(2,3,i+1)
#    labels = [item.get_text() for item in ax.get_yticklabels()]
#    labels = lables/1.0e+6
#    ax.set_yticklabels(labels)
    plt.xlim((0.0, 0.5*np.pi))
    ax.set_xticks([0.0, 0.25*np.pi, 0.5*np.pi])
    ax.set_xticklabels([r'$0$', r'$\pi/4$', r'$\pi/2$'])
    ax.set_yticks([0.0, 1.0e+6, 2.0e+6, 3.0e+6, 4.0e+6])
    ax.set_yticklabels([r'$0$', r'$1.0$', r'$2.0$', r'$3.0$', r'$4.0$'])
    plt.ylim((0.0,4.2e+6))
    plt.plot(theta,chi22, '--')
    plt.plot(theta,chi33, '-')
    plt.title(r'$t = %.2f$' % (t))
#    plt.legend([r'$\chi_{22}(t = %.2f)$' % (t), r'$\chi_{33}(t = %.2f)$' % (t)], loc=0)
plt.savefig('C2_chis.pdf')
plt.show()

# Plots of principal curvatures (lambdas) times u
fig = plt.figure()
fig.subplots_adjust(hspace=.5)

for i in range(LEN):
    t = float(datos[12*i])
    theta = datos[12*i+1].split()
    ulp = datos[12*i+10].split()
    ult = datos[12*i+11].split()
    #plot de la curvatura extrinseca
    ax = fig.add_subplot(2,3,i+1)
#    labels = [item.get_text() for item in ax.get_yticklabels()]
#    labels = lables/1.0e+6
#    ax.set_yticklabels(labels)
    plt.xlim((0.0, 0.5*np.pi))
    ax.set_xticks([0.0, 0.25*np.pi, 0.5*np.pi])
    ax.set_xticklabels([r'$0$', r'$\pi/4$', r'$\pi/2$'])
    ax.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
    ax.set_yticklabels([r'$0$', r'$0.2$', r'$0.4$', r'$0.6$', r'$0.8$', r'$1.0$'])
    plt.ylim((0.0,1.0))
    plt.plot(theta,ulp, '--')
    plt.plot(theta,ult, '-')
    plt.title(r'$t = %.2f$' % (t))
#    plt.legend([r'$\lambda_phi(t = %.2f)$' % (t), r'$\lambda_\theta(t = %.2f)$' % (t)], loc=0)
plt.savefig('C2_ul.pdf')
plt.show()


lambda_phi_inicial = datos[8].split()
lambda_theta_inicial = datos[9].split()
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set_xticks([0.0, 0.25*np.pi, 0.5*np.pi])
ax.set_xticklabels([r'$0$', r'$\pi/4$', r'$\pi/2$'])
plt.xlim((0.0, 0.5*np.pi))
plt.ylim((6.0e-07,7.5e-07))
plt.plot(theta,lambda_phi_inicial, '--')
plt.plot(theta,lambda_theta_inicial, '-')
plt.ylabel(r'$[{\rm cm}^{-1}]$')
#plt.legend([r'$\lambda_\phi$',r'$\lambda_\theta$'], loc=0)
plt.savefig('C2_lambdas_init.pdf')
plt.show()



