{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Región de estabilidad de los métodos de integración \n",
    "#### Joaquín Pelle\n",
    "\n",
    "### Introducción\n",
    "\n",
    "Consideremos un problema de valores iniciales\n",
    "\n",
    "\\begin{align*}\n",
    "\\frac{du}{dt} &= f(u,t) \\,, \\\\\n",
    "u(t_0) &= u_0\\,,\n",
    "\\end{align*}\n",
    "\n",
    "con $u\\colon I \\to \\mathbb{C}$, donde $I \\subset \\mathbb{R}$ es un intervalo. Los llamados *métodos de integración de un paso* son algoritmos de la forma\n",
    "\n",
    "\\begin{align*}\n",
    "v_{n+1} &= v_n + \\Delta t F(v_{n},t,\\Delta t)\\,, \\\\\n",
    "v_0 &= u_0\\,,\n",
    "\\end{align*}\n",
    "\n",
    "con $t_n = t_0+n \\Delta t$, donde $v_{n}$ es el valor aproximado de $u(t_n)$. Cada método corresponderá a una distinta función $F(v_{n},t,\\Delta t)$. Para definir la región de estabilidad de un método, consideramos el problema particular\n",
    "\n",
    "\\begin{align}\n",
    "\\frac{du}{dt} &= \\lambda u\\,, \\\\\n",
    "u(t_0) &= u_0\\,,\n",
    "\\end{align}\n",
    "\n",
    "con $\\lambda \\in \\mathbb{C}$, cuya solución es\n",
    "\n",
    "\\begin{align}\n",
    "u(t) &= u_0 e^{\\lambda (t-t_0)} \\,\n",
    "\\end{align}\n",
    "\n",
    "(notemos que si $\\mathrm{Re}(\\lambda) \\leq 0$, el módulo de la solución no puede crecer). En el caso de esta ecuación, muchos de los métodos de un paso toman la forma\n",
    "\n",
    "\\begin{align*}\n",
    "v_{n+1} &= [ 1+ A(\\lambda \\Delta t) ] v_n \\,, \\\\\n",
    "v_0 &= u_0\\,,\n",
    "\\end{align*}\n",
    "\n",
    "donde $A$ es una función que sólo depende del producto $\\lambda \\Delta t$. Esta subclase de métodos de un paso es suficientemente general como para incluir a los más comunes, como los de Euler, Heun, Runge-Kutta, etc. Bajo estas condiciones, podemos definir la **región de estabilidad** del método como la región\n",
    "\n",
    "\\begin{equation*}\n",
    "S = \\{ \\mu \\in \\mathbb{C}: |1+A(\\mu)| \\leq 1  \\}\\,.\n",
    "\\end{equation*}\n",
    "\n",
    "En otras palabras, es la región donde el factor de amplificación del método para la ecuación $u_t = \\lambda u$ es menor o igual que $1$.\n",
    "\n",
    "Incluso si la solución exacta se mantiene acotada ($\\mathrm{Re}(\\lambda) \\leq 0$), el factor de amplificación de un método puede resultar mayor que $1$ si el paso $\\Delta t$ no es suficientemente pequeño. En ese caso los valores $v_n$ se volverán arbitrariamente grandes, y la aproximación numérica no será útil. Por lo tanto, de ser posible, debemos elegir un paso temporal lo suficientemente pequeño como para que $\\lambda \\Delta t$ se encuentre dentro de la región de estabilidad del método, de modo que la solución numérica no diverja. \n",
    "\n",
    "A pesar de que la región de estabilidad está definida según el comportamiento del método para la ecuación $u_t = \\lambda u$, el concepto tiene importancia en general. Si queremos resolver una  ecuación genérica \n",
    "\n",
    "\\begin{align*}\n",
    "\\frac{du}{dt} &= f(u,t) \\,, \\\\\n",
    "u(t_0) &= u_0\\,,\n",
    "\\end{align*}\n",
    "\n",
    "con algún método, buscaremos que el paso temporal sea lo suficientemente pequeño como para que $\\lambda_n \\Delta t$ se encuentre dentro de la región de estabilidad, donde ahora\n",
    "\\begin{equation}\n",
    "\\lambda_n = \\frac{\\partial f}{\\partial y}(v_n,t_n)\\,\n",
    "\\end{equation}\n",
    "(esto puede conducir a la necesidad de un método adaptativo).\n",
    "\n",
    "\n",
    "A modo de ejemplo, consideremos el método de Euler explícito, que para la ecuación $u_t = \\lambda u$ se reduce simplemente a \n",
    "\n",
    "\\begin{align*}\n",
    "v_{n+1} &= (1+ \\lambda \\Delta t) v_n \\,, \\\\\n",
    "v_0 &= u_0\\,.\n",
    "\\end{align*}\n",
    "\n",
    "Luego, la región de estabilidad es\n",
    "\\begin{equation}\n",
    "S_{\\mathrm{Euler}} = \\{ \\mu \\in \\mathbb{C}: |1+\\mu| \\leq 1  \\}\\,,\n",
    "\\end{equation}\n",
    "\n",
    "es decir, el círculo de radio $1$ centrado en $-1$. Por ejemplo, para la ecuación $u_t =-5u$ no podemos tomar $\\Delta t > 2/5$, pues $\\lambda \\Delta t$ caería fuera de la región de estabilidad y las aproximaciones sucesivas por el método de Euler crecerían sin cota, a pesar de que la solución exacta decae como $e^{-5t}$. Aún peor, para la ecuación $u_t = i u$, cuya solución exacta oscila con módulo constante, el método de Euler resulta inestable para cualquier $\\Delta t > 0$.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cálculo numérico de las regiones de estabilidad\n",
    "\n",
    "En este notebook encontrarán funciones que permiten graficar la región de estabilidad de un método numérico. En la práctica, si tenemos un algoritmo numérico, a partir de la relación\n",
    "\n",
    "\\begin{align*}\n",
    "v_{n+1} &= [ 1+ A(\\lambda \\Delta t) ] v_n \\,, \\\\\n",
    "v_0 &= u_0\\,,\n",
    "\\end{align*}\n",
    "\n",
    "el factor de amplificación se puede obtener como\n",
    "\n",
    "\\begin{equation}\n",
    "1+ A(\\lambda \\Delta t) = \\frac{|v_1|}{|v_0|}\\,.\n",
    "\\end{equation} \n",
    "\n",
    "Es decir, para saber si $\\lambda \\Delta t$ se encuentra en la región de estabilidad de un método, basta con dar el paso inicial de la ecuación $u_t=\\lambda u$ con ese método. Más aún, sin pérdida de generalidad podemos tomar condición inicial $v_0=1$ y paso temporal $\\Delta t=1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Importamos los paquetes necesarios"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using OrdinaryDiffEq\n",
    "using Plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Definimos la función a integrar, $f(u,t ; \\lambda)=\\lambda u$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f(u,λ,t) = λ*u"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La siguiente función recibe un método de integración y un valor de $\\mu = \\lambda \\Delta t$ y realiza un paso de la ecuación $u_t = \\lambda u$ con condición inicial $u_0=1$ y paso $\\Delta t = 1$. Con ello calcula el factor de amplificación como $|v_1|$, retornando $1$ si $|v_1|>1$ o $0$ en caso contrario. (Concretamente, es la función característica de la región de estabilidad.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function indicador_estabilidad(μ,metodo)\n",
    "    \n",
    "    # Condición inicial\n",
    "    u0 = 1.0+0.0im\n",
    " \n",
    "    # Intervalo de tiempo\n",
    "    tspan = (0.0, 1.0)\n",
    "    \n",
    "    # Seteamos el problema  (tomamos λ = μ)\n",
    "    prob = ODEProblem(f,u0,tspan,μ)\n",
    "    \n",
    "    # Inicializamos el integrador y damos un paso\n",
    "    # (Lo hacemos de esta manera porque sólo nos interesa dar un paso)\n",
    "    \n",
    "    integrator = init(prob, metodo, dt=1.0, adaptive=false)\n",
    "    step!(integrator)\n",
    "    \n",
    "    #Tomamos el valor de la función después del paso\n",
    "    uf = integrator.u\n",
    "        \n",
    "    return abs(uf) > 1. ? 0.0 : 1.0 \n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Estos son algunos métodos que pueden probar: `Euler()`, `Heun()`, `RK4()`, `Tsit5()`, `SSPRK33()`, `SSPRK54()`. La información sobre los métodos, y la lista completa de los métodos disponibles se encuentra en https://diffeq.sciml.ai/stable/solvers/ode_solve/. Algunos de ellos pueden requerir los paquetes adicionales `Sundials` o `LSODA`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finalmente, graficamos las regiones de estabilidad de varios métodos"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "default(size=(600,900), fc=:heat)\n",
    "x = -5.5:0.02:0.5\n",
    "y = -5:0.02:5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Graficamos la función que toma un par $(x,y)$ y devuelve $1$ si $x+iy \\in S$, o devuelve $0$ en caso contrario, donde $S$ es la región de estabilidad del método."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "heatmap(x,y,(x,y)->indicador_estabilidad(x+y*1.0im,Euler()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "heatmap(x,y,(x,y)->indicador_estabilidad(x+y*1.0im,RK4()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "heatmap(x,y,(x,y)->indicador_estabilidad(x+y*1.0im,Heun()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "heatmap(x,y,(x,y)->indicador_estabilidad(x+y*1.0im,Tsit5()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "heatmap(x,y,(x,y)->indicador_estabilidad(x+y*1.0im,SSPRK33()))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.6.1",
   "language": "julia",
   "name": "julia-1.6"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
