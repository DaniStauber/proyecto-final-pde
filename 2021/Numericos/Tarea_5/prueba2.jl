function make_model(p)
    gmsh.initialize()
    gmsh.option.setNumber("General.Terminal", 1)

    name ,xcini, ycini, zcini, dx, dy, dz, xsc, ysc, zsc, rs, lc = p
    gmsh.model.add("$name")

    #vertices del cubo
    gmsh.model.geo.addPoint(xcini, 0, zcini, lc, 1)
    gmsh.model.geo.addPoint(xcini, ycini,  zcini, lc, 2)
    gmsh.model.geo.addPoint(0, ycini, zcini, lc, 3)
    gmsh.model.geo.addPoint(0, 0, zcini, lc, 4)
    gmsh.model.geo.addPoint(xcini + dx , 0, zcini + dz, lc, 5)
    gmsh.model.geo.addPoint(xcini + dx, ycini +dy,  zcini + dz, lc, 6)
    gmsh.model.geo.addPoint(0, ycini +dy,  zcini + dz, lc, 7)
    gmsh.model.geo.addPoint(0, 0, zcini + dz, lc, 8)
    
    # make the box boundary
    gmsh.model.geo.addLine(1, 2, 1)
    gmsh.model.geo.addLine(2, 3, 2)
    gmsh.model.geo.addLine(3, 4, 3)
    gmsh.model.geo.addLine(4, 1, 4)
    
    gmsh.model.geo.addLine(1, 5, 5)
    gmsh.model.geo.addLine(2, 6, 6)
    gmsh.model.geo.addLine(3, 7, 7)
    gmsh.model.geo.addLine(4, 8, 8)
    
    gmsh.model.geo.addLine(5, 6, 9)
    gmsh.model.geo.addLine(6, 7, 10)
    gmsh.model.geo.addLine(7, 8, 11)
    gmsh.model.geo.addLine(8, 5, 12)
    
    gmsh.model.geo.addCurveLoop([1, 6, 9, 5], 10) #cara  1
    gmsh.model.geo.addCurveLoop([2, 7, 10, 6], 11) #cara  2
    gmsh.model.geo.addCurveLoop([3, 8, 11, 7], 12) #cara  3
    gmsh.model.geo.addCurveLoop([4, 5, 12, 8], 13) #cara  4
    gmsh.model.geo.addCurveLoop([1, 2, 3, 4], 14) #cara  5
    gmsh.model.geo.addCurveLoop([9, 10, 11, 12], 15) #cara  4
    gmsh.model.geo.synchronize()

    gmsh.model.geo.addPlaneSurface([10,11,12,13,14,15], 100) #the surface
    gmsh.model.geo.synchronize()
    
    gmsh.model.addPhysicalGroup(2, [100], 101)
    gmsh.model.setPhysicalName(2, 101, "ext")
    gmsh.model.geo.synchronize()
    
    
    gmsh.model.geo.addPoint(cy_center_x, cy_center_y, 0, lc_f, 5)
    gmsh.model.geo.addPoint(cy_center_x + cy_radious, cy_center_y, 0, lc_f, 6)
    gmsh.model.geo.addPoint(cy_center_x , cy_center_y + cy_radious, 0, lc_f, 7)
    gmsh.model.geo.addPoint(cy_center_x - cy_radious, cy_center_y, 0, lc_f, 8)
    gmsh.model.geo.addPoint(cy_center_x, cy_center_y - cy_radious, 0, lc_f, 9)

    gmsh.model.geo.addCircleArc(6,5,7,5)
    gmsh.model.geo.addCircleArc(7,5,8,6)
    gmsh.model.geo.addCircleArc(8,5,9,7)
    gmsh.model.geo.addCircleArc(9,5,6,8)

    gmsh.model.geo.addCurveLoop([5, 6, 7, 8], 12) #the circle
    gmsh.model.geo.synchronize()