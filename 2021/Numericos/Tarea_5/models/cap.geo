//
lc = 1e-1/2;
Point(1)={ 0, 0, 0, lc};
Point(2)={20, 0,  0, lc};
Point(3)={20.0, 20.0, 0, lc};
Point(4)={0, 20.0, 0, lc};


Point(5)={ 7.5, 7., 0, lc};
Point(6)={12.5, 7.,  0, lc};
Point(7)={12.5, 8., 0, lc};
Point(8)={7.5, 8., 0, lc};


Point(9)={ 7.5, 12., 0, lc};
Point(10)={12.5, 12.,  0, lc};
Point(11)={12.5, 13., 0, lc};
Point(12)={7.5, 13., 0, lc};



//# make the square boundary ext
Line(1)={1, 2};
Line(2)={2, 3};
Line(3)={3, 4};
Line(4)={4, 1};

//# make the square inf boundary 
Line(5)={5, 6};
Line(6)={6, 7};
Line(7)={7, 8};
Line(8)={8, 5};

//# make the square sup boundary 
Line(9)={9, 10};
Line(10)={10, 11};
Line(11)={11, 12};
Line(12)={12, 9};

// #the rectangle ext

Curve Loop(20)={1, 2, 3, 4};

// #the rectangle sup

Curve Loop(21)={9, 10, 11, 12};

// #the rectangle inf

Curve Loop(22)={5, 6, 7, 8};

Plane Surface(100)={20,21,22};

//# make the surface

Physical Surface("surface",4) = {100};

Physical Curve("ext",3) = {1,2,3,4};

Physical Curve("sup",2) = {9, 10, 11, 12};

Physical Curve("inf",1) = {5, 6, 7, 8};

Mesh 2;

Save "cap.msh";