lc = 1e-2;
Point(1)={ 0, 0, 0, lc};
Point(2)={10.0, 0,  0, lc};
Point(3)={10.0, 8.0, 0, lc};
Point(4)={0, 8.0, 0, lc};
//# now points for a inner circle
lc_f = 1e-3;
Point(5)={5., 3., 0, lc_f};
Point(6)={6., 4., 0, lc_f};
Point(7)={5., 5., 0, lc_f};
Point(8)={4., 4., 0, lc_f};
Point(9)={5., 4., 0, lc_f};


Point(10)={5., 1., 0, lc_f};
Point(11)={8., 4., 0, lc_f};
Point(12)={5., 7., 0, lc_f};
Point(13)={2., 4., 0, lc_f};


//# make the square boundary
Line(1)={1, 2};
Line(2)={2, 3};
Line(3)={3, 4};
Line(4)={4, 1};

//# make the circle
Circle(5)={5,9,6};
Circle(6)={6,9,7};
Circle(7)={7,9,8};
Circle(8)={8,9,5};

//# make the circle
Circle(10)={10,9,11};
Circle(11)={11,9,12};
Circle(12)={12,9,13};
Circle(13)={13,9,10};

Curve Loop(14)={1, 2, 3, 4};
// #the rectangle

Curve Loop(15)={5, 6, 7, 8}; 
//#the inner circle

Curve Loop(16)={10, 11, 12,13}; 
//#the exterior circle

Plane Surface(100)={14,15};
//#the surface


//# make the surface



Physical Curve("ext") = {1,2,3,4};

Physical Curve("circuloint") = {5,6,7,8};

Physical Curve("circuloext") = {10,11,12,13}; 

Mesh 2;

Save "circulos.msh";
