//+
SetFactory("OpenCASCADE");
Box(1) = {-100, -100, -100, 200, 200, 200};
//+
Sphere(2) = {0., 0., 0., 10., -Pi/2, Pi/2, 2*Pi};

//+
Surface Loop(3) = { 1, 2, 3, 4,5,6};
//+
Volume(6) = {1,2,3,4,5,6};
//+
Surface Loop(4) = {7};
//+
Volume(7) = {7};


//+
BooleanDifference(8) = { Volume{1,2,3,4,5,6,7}; Delete; }{ Volume{7}; Delete; };
//+
Physical Volume("volume") = {5};

